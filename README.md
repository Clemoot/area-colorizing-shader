# Area Colorizing Shader

This Unity shader restricts the area where the space is colorized. There are points in the scene that acts like lights. The sphere around these points defines the space where objects are colorized.

Parameters:
- Origin of the sphere
- Radius of the sphere
- Outline thickness

![](sample.jpg)