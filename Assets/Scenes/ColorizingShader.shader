// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/ColorizingShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        Origin ("Origin", Vector) = (0, 0, 0)
        Radius ("Radius", Float) = 0
        OutlineThickness ("Outline Thickness", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : POSITION0;
                float4 worldPos : POSITION1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float3 Origin;
            float Radius;
            float OutlineThickness;

            v2f vert (appdata v)
            {
                v2f o;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                float d = length(i.worldPos - float4(Origin, 1.0f)); 
                if (d > Radius - OutlineThickness) {
                    if (d < Radius + OutlineThickness)
                        return fixed4(1.0, 1.0, 1.0, 1.0);
                    return fixed4(0.0, 0.0, 0.0, 1.0);
                }
                return col;
            }
            ENDCG
        }
    }
}
