using System;
using System.Collections.Generic;
using UnityEngine;

// Author : Cl�ment GARNIER

public class ColorizingPoint : MonoBehaviour
{
    private const String ORIGIN_NAME = "Origin";
    private const String RADIUS_NAME = "Radius";

    public Shader colorizingShader;
    private List<Renderer> _renderers;

    // Start is called before the first frame update
    void Start()
    {
        Renderer[] renderers = FindObjectsOfType<Renderer>();
        _renderers = new List<Renderer>();
        foreach (Renderer r in renderers)
        {
            if (r.material.shader.name == colorizingShader.name)
            {
                r.material.SetVector(ORIGIN_NAME, transform.position);
                _renderers.Add(r);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.hasChanged)
        {
            foreach (Renderer r in _renderers)
                r.material.SetVector(ORIGIN_NAME, transform.position);
        }
        float radius = (1.0f + Mathf.Sin(Time.realtimeSinceStartup)) * 20.0f;
        foreach (Renderer r in _renderers)
            r.material.SetFloat(RADIUS_NAME, radius);
    }
}
